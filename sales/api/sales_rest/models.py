from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)
    import_href = models.CharField(max_length=200, unique=True)


class Salesperson(models.Model):
    first_name = models.CharField(max_length=25)
    last_name = models.CharField(max_length=25)
    employee_id = models.CharField(max_length=25)

    def get_api_url(self):
        return reverse("api_list_salespeople", kwargs={"id": self.id})


class Customer(models.Model):
    first_name = models.CharField(max_length=25)
    last_name = models.CharField(max_length=25)
    address = models.TextField()
    phone_number = models.BigIntegerField(unique=True, blank=True)

    def get_api_url(self):
        return reverse("api_list_customers", kwargs={"id": self.id})


class Sale(models.Model):
    price = models.PositiveIntegerField(null=True)

    salesperson = models.ForeignKey(
        Salesperson,
        related_name="salesperson",
        on_delete=models.PROTECT,
    )

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.CASCADE,
    )

    customer = models.ForeignKey(
        Customer,
        related_name="customer",
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_list_sales", kwargs={"id": self.id})


