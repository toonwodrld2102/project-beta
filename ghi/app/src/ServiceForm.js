import React, {useEffect, useState} from 'react';

function ServiceForm() {
    const [technicians, setTechnicians] = useState([]);
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [reason, setReason] = useState('');
    const [tech, setTech] = useState('');
    const [status, setStatus] = useState('Created');

    const handleTechChange = (event) => {
        const value = event.target.value;
        setTech(value);
    }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    }

    const handleTimeChange = (event) => {
        const value = event.target.value;
        setTime(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setTechnicians(data.technician);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.vin = vin;
        data.customer = customer;
        data.date = date;
        data.time = time;
        data.reason = reason;
        data.technician = tech;
        data.status = status;

        const appointmentUrl = `http://localhost:8080/api/appointments/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json'},
        };

        const response = await fetch(appointmentUrl, fetchConfig);
        console.log(response)

        if (response.ok) {
            const newAppointment = await response.json();
            console.log(newAppointment);

            setTech('');
            setReason('');
            setDate('');
            setTime('');
            setCustomer('');
            setVin('');
            setStatus('Created')

        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
        <div className="my-5 container">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create an Appointment</h1>
            <form onSubmit={handleSubmit} id="create-service-form">
                <div className="form-floating mb-3">
                <input onChange={handleVinChange} placeholder="Automobile Vin" name='vin' value={vin} type="text" id="vin" className="form-control"/>
                <label htmlFor="vin">Automobile Vin</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleCustomerChange} placeholder="Customer" name='customer' value={customer} type="text" id="customer" className="form-control"/>
                <label htmlFor="customer">Customer</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleDateChange} placeholder="Date" name='date' value={date} type="date" id="date" className="form-control"/>
                <label htmlFor="date">Date of appointments</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleTimeChange} placeholder="Time" name='time' value={time} type="time" id="time" className="form-control"/>
                <label htmlFor="time">Time of appointments</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleReasonChange} placeholder="Reason" name='reason' value={reason} type="text" id="reason" className="form-control"/>
                <label htmlFor="reason">Reason</label>
                </div>
                <div className="mb-3">
                <select name="tech" onChange={handleTechChange} value={tech} id="tech" className="form-select">
                    <option value="">Choose a Technician</option>
                    {technicians.map(technician => {
                        return (
                            <option key={technician.employee_id} value={technician.employee_id}>
                            {technician.first_name + " " + technician.last_name}
                            </option>
                        );
                    })}
                </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
        </>
    )
}

export default ServiceForm;
