import { useEffect, useState } from 'react';


function CustomerList() {

    const [customers, setCustomers] = useState([]);

    async function getCustomers() {
        const customerUrl = 'http://localhost:8090/api/customers/';
        const response = await fetch(customerUrl)
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        };

    }

    useEffect(() => {
        getCustomers();
    }, [] );


    return (

        <table className="table table-striped table-hover">
          <thead className='thead table-success'>
            <tr>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Address</th>
              <th>Phone Number</th>
            </tr>
          </thead>
          <tbody>
          {customers.map(customer => {
            return (
            <tr key={customer.id}>
              <td>{ customer.first_name }</td>
              <td>{ customer.last_name}</td>
              <td>{ customer.address}</td>
              <td>{ customer.phone_number}</td>
            </tr>

          );
        })}
          </tbody>
        </table>
        );


}

export default CustomerList;
