import React, {useEffect, useState} from "react";


function ServiceHistory() {
    const [appointments, setAppointment] = useState([]);
    const [filteredData, setFilteredData] = useState([])

    const fetchData = async () => {
        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const response = await fetch(appointmentUrl);
        if (response.ok) {
            const data = await response.json();
            setAppointment(data.appointment);
        }
    }

    const handleFilter = (event) => {
    const searchVin = event.target.value
    setFilteredData(searchVin)
    };

    let filter = appointments;
    if (filteredData.length > 0) {
        filter = appointments.filter(service => service.vin.toUpperCase().includes(filteredData.toUpperCase()))
    }


    useEffect(() => {
        fetchData();
    }, [])

    return (
        <>
        <h1>Service History</h1>
        <div className="input-group mb-3">
            <input type="text" className="form-control" aria-label="Search for VIN" aria-describedby="button-addon2" onChange={handleFilter} placeholder="Search for vin" value={filteredData} data={appointments} />
            <button className="btn btn-outline-secondary" type="button" id="button-addon2">Search</button>
        </div>
        <table className="table table-striped-columns">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>is VIP?</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                {filter.map(service => {
                    return (
                        <tr key={service.id}>
                            <td>{service.vin}</td>
                            <td>{service.vip ? "Yes" : "No" }</td>
                            <td>{service.customer}</td>
                            <td>{service.date}</td>
                            <td>{service.time}</td>
                            <td>{service.technician.first_name + " " + service.technician.last_name}</td>
                            <td>{service.reason}</td>
                            <td>{service.status}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    )
}


export default ServiceHistory;
