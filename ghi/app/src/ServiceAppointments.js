import React, {useEffect, useState} from "react";


function ServiceAppointments() {
    const [appointments, setAppointment] = useState([]);

    const fetchData = async () => {
        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const response = await fetch(appointmentUrl);
        if (response.ok) {
            const data = await response.json();
            setAppointment(data.appointment);
        }
    }

    const handleFinish = async (service) => {
        const finishUrl = `http://localhost:8080/api/appointments/${service.id}/finish/`;
        const fetchConfig = {
            method: "put",
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(finishUrl, fetchConfig);
        if (response.ok) {
            fetchData();
        }

    }

    const handleCancel = async (service) => {
        const cancelUrl = `http://localhost:8080/api/appointments/${service.id}/cancel/`;
        const fetchConfig = {
            method: "put",
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(cancelUrl, fetchConfig);
        if (response.ok) {
            fetchData();
        }
    }

    useEffect(() => {
        fetchData();
    }, [])
    return (
        <>
        <h1>Service Appointments</h1>
        <table className="table table-striped-columns">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>is VIP?</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                </tr>
            </thead>
            <tbody>
                {appointments.map(service => {
                    if (service.status === "Created") {
                        return (
                            <tr key={service.id}>
                                <td>{service.vin}</td>
                                <td>{service.vip ? "Yes" : "No" }</td>
                                <td>{service.customer}</td>
                                <td>{service.date}</td>
                                <td>{service.time}</td>
                                <td>{service.technician.first_name + " " + service.technician.last_name}</td>
                                <td>{service.reason}</td>
                                <td>
                                    <button onClick={() => handleCancel(service)} value={service.id} type="button" className="btn btn-danger">Cancel</button>
                                    <button onClick={() => handleFinish(service)} value={service.id} type="button" className="btn btn-success">Finish</button>
                                </td>
                            </tr>
                        );
                    }
                })}
            </tbody>
        </table>
        </>
    )
}


export default ServiceAppointments;
