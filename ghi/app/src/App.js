import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import VehicleForm from './VehicleForm';
import VehicleModelList from './VehicleModelList';
import AutoList from './AutoList';
import AutoForm from './AutoForm';
import TechnicianList from './TechnicianList'
import TechnicianForm from './TechnicianForm'
import ServiceForm from './ServiceForm';
import ServiceAppointments from './ServiceAppointments';
import ServiceHistory from './ServiceHistory';
import SalespersonForm from './SalespersonForm';
import SalespersonList from './SalespersonList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import NewSaleForm from './NewSaleForm';
import SalesList from './SalesList';
import SalespersonHistory from './SalespersonHistory';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route path="new" element={<ManufacturerForm />} />
            <Route index element={<ManufacturerList />} />
          </Route>
          <Route path="models">
            <Route path="new" element={<VehicleForm />} />
            <Route index element={<VehicleModelList />} />
          </Route>
          <Route path="automobiles">
            <Route path="new" element={<AutoForm />} />
            <Route index element={<AutoList />} />
          </Route>
          <Route path="technicians">
            <Route path="new" element={<TechnicianForm />} />
            <Route index element={<TechnicianList />} />
          </Route>
          <Route path="appointments">
            <Route path="new" element={<ServiceForm />} />
            <Route index element={<ServiceAppointments />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>
          <Route path="salespeople">
            <Route path="new" element={<SalespersonForm />} />
            <Route index element={<SalespersonList />} />
          </Route>
          <Route path="customers">
            <Route path="new" element={<CustomerForm />} />
            <Route index element={<CustomerList />} />
          </Route>
          <Route path="sales">
            <Route path="new" element={<NewSaleForm />} />
            <Route path="history" element={<SalespersonHistory />} />
            <Route index element={<SalesList />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
