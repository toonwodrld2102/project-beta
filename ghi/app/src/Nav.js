import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>

        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarNavDropdown">
          <ul className="navbar-nav">

          <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Inventory
            </a>
            <ul className="dropdown-menu">
              <NavLink className="dropdown-item" to="/manufacturers/new">Create Manufacturer</NavLink>
              <NavLink className="dropdown-item" to="/manufacturers">List of Manufacturers</NavLink>
              <NavLink className="dropdown-item" to="/models/new">Create a Vehicle Model</NavLink>
              <NavLink className="dropdown-item" to="/models">List of Vehicle Models</NavLink>
              <NavLink className="dropdown-item" to="/automobiles/new">add a Automobile</NavLink>
              <NavLink className="dropdown-item" to="/automobiles">List of Automobiles</NavLink>
            </ul>
          </li>

          <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Service
            </a>
            <ul className="dropdown-menu">
              <NavLink className="dropdown-item" to="/technicians">List of Technicians</NavLink>
              <NavLink className="dropdown-item" to="/technicians/new">Add a Technician</NavLink>
              <NavLink className="dropdown-item" to="/appointments">Service Appointments</NavLink>
              <NavLink className="dropdown-item" to="/appointments/history">Service History</NavLink>
              <NavLink className="dropdown-item" to="/appointments/new">Create a Appointment</NavLink>
            </ul>
          </li>

          <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Sales
            </a>
            <ul className="dropdown-menu">
              <NavLink className="dropdown-item" to="/salespeople/new">Create Salesperson</NavLink>
              <NavLink className="dropdown-item" to="/salespeople">List of Salespeople</NavLink>
              <NavLink className="dropdown-item" to="/customers/new">Create Customer</NavLink>
              <NavLink className="dropdown-item" to="/customers">List of Customers</NavLink>
              <NavLink className="dropdown-item" to="/sales/new">Create New Sale</NavLink>
              <NavLink className="dropdown-item" to="/sales">List of Sales</NavLink>
              <NavLink className="dropdown-item" to="/sales/history">Salesperson Sales History</NavLink>
            </ul>
          </li>

          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
