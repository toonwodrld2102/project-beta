import React, {useEffect, useState} from "react";


function TechnicianList() {
    const [technician, setTechnician] = useState([]);
    const fetchData = async () => {
        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const response = await fetch(technicianUrl);
        if (response.ok) {
            const data = await response.json();
            setTechnician(data.technician);
        }
    }
    useEffect(() => {
        fetchData();
    }, [])
    return (
        <>
        <h1>Technicians</h1>
        <table className="table table-striped-columns">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Employee ID</th>
                </tr>
            </thead>
            <tbody className="col">
                {technician.map(tech => {
                    return (
                        <tr key={tech.id}>
                            <td>{tech.first_name}</td>
                            <td>{tech.last_name}</td>
                            <td>{tech.employee_id}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    )
}


export default TechnicianList;
