import { useEffect, useState } from 'react';


function SalespersonList() {

    const [salespeople, setSalespeople] = useState([]);

    async function getSalespeople() {
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(salespeopleUrl)
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople)
        };
    }

    useEffect(() => {
        getSalespeople();
    }, [] );

    return (

        <table className="table table-striped table-hover">
          <thead className='thead table-success'>
            <tr>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Employee Id</th>
            </tr>
          </thead>
          <tbody>
          {salespeople.map(salesperson => {
            return (
            <tr key={salesperson.id}>
              <td>{ salesperson.first_name }</td>
              <td>{ salesperson.last_name}</td>
              <td>{ salesperson.employee_id}</td>
            </tr>

          );
        })}
          </tbody>
        </table>
        );


}

export default SalespersonList;
