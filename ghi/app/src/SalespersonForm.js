import React, { useState } from 'react';

function SalespersonForm() {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');

    const handleFirstNameChange = (event) => {
        setFirstName(event.target.value);
    }

    const handleLastNameChange = (event) => {
        setLastName(event.target.value);
    }

    const handleEmployeeIdChange = (event) => {
        setEmployeeId(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeId;

    const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        }
    };

    const response = await fetch(salespeopleUrl, fetchConfig);
    if (response.ok) {
        const newSalesperson = await response.json();

        setFirstName('');
        setLastName('');
        setEmployeeId('');

    }

    };

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create New Salesperson</h1>
            <form onSubmit={handleSubmit}
            id="create-salesperson-form">

                <div className="form-floating mb-3">
                <input onChange={handleFirstNameChange}
                placeholder="first_name"
                required type="text"
                name="first_name"
                id="first_name"
                value={firstName}
                className="form-control" />
                <label htmlFor="first_name">First Name</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleLastNameChange}
                placeholder="last_name"
                required type="text"
                name="last_name"
                id="last_name"
                value={lastName}
                className="form-control" />
                <label htmlFor="last_name">Last Name</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleEmployeeIdChange}
                placeholder="employee_id"
                required type="text"
                name="employee_id"
                id="employee_id"
                value={employeeId}
                className="form-control" />
                <label htmlFor="employee_id">Employee ID</label>
              </div>

              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );



}

export default SalespersonForm;
