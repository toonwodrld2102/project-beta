import React, {useState} from "react";



function SearchBar({ placeholder, data }) {
  const [filteredData, setFilteredData] = useState([])

  const handleFilter = (event) => {
    const searchVin = event.target.value
    const filteredVin = data.filter((value) => {
      return value.vin.toLowerCase().includes(searchVin.toLowerCase());
    });

    if (searchVin === ""){
      setFilteredData([])
    } else {
      setFilteredData(filteredVin)
    }
  }

  return (
    <div className="search">
      <div className="searchInputs">
        <input
          type="text"
          placeholder={placeholder}
          onChange={handleFilter}
        />
      </div>
      {filteredData.length != 0 && (
      <div className="dataResult">
        {filteredData.slice(0, 15).map((value, key) => {
          return (
            <div>
              <p>{value.vin}</p>
            </div>
          );
        })}
      </div>
      )}
    </div>
  );
}

export default SearchBar;
